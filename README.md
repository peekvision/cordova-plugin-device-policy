# cordova-plugin-device-policy

This plugin defines a global `devicepolicy` object, which provides access to the device's security policy management.
Although the object is in the global scope, it is not available until after the `deviceready` event.

```js
document.addEventListener("deviceready", onDeviceReady, false);
function onDeviceReady() {
    window['devicepolicy'].getStatus(result => console.log(result), err => console.log(err));
}
```

Report issues with this plugin on the [Project issue tracker](https://bitbucket.org/peekvision/cordova-plugin-device-policy/issues)

## Installation

    cordova plugin add cordova-plugin-device-policy
    or
    cordova plugin add https://bitbucket.org/peekvision/cordova-plugin-device-policy.git
    or
    node_modules/.bin/cordova plugin add https://bitbucket.org/peekvision/cordova-plugin-device-policy.git

## Functions

- devicepolicy.getStatus
- devicepolicy.setAdminActive
- devicepolicy.setPasswordSufficient
- devicepolicy.setStorageEncryption

## Supported Platforms

- Android

## Quick Examples

```js
window['devicepolicy'].getStatus(result => console.log(result), err => console.log(err));

window['devicepolicy'].setAdminActive(result => console.log(result), err => console.log(err));

window['devicepolicy'].setPasswordSufficient(result => console.log(result), err => console.log(err));

window['devicepolicy'].setStorageEncryption(result => console.log(result), err => console.log(err));
```

Result:
```js
result.encryptionStatus
result.isPasswordSufficient
result.isAdminActive
```

## Android Quirks

- Calls the Android [DevicePolicyManager](https://developer.android.com/reference/android/app/admin/DevicePolicyManager.html) 

- Encryption Statuses:
```js
UNSUPPORTED = 0,
INACTIVE =    1,
ACTIVATING =  2,
ACTIVE =      3,
ACTIVE_DEFAULT_KEY = 4,
ACTIVE_PER_USER = 5;
```

# Licence
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.


