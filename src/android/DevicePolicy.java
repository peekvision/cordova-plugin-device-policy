/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
*/
package org.peekvision.cordova.devicepolicy;

import org.apache.cordova.CordovaWebView;

import java.util.Arrays;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class DevicePolicy extends CordovaPlugin {
    private static final String TAG = "DevicePolicy";

    private static final int REQUEST_ADMIN_ACTIVE = 1;
    private static final int REQUEST_NEW_PASSWORD = 2;
    private static final int REQUEST_STORAGE_ENCRYPTION = 3;

    private CordovaInterface cordova;
    private ComponentName componentName;
    private CallbackContext callbackContext;
    private DevicePolicyManager devicePolicyManager;
    private PackageManager packageManager;

    private int REQUIRED_PASSWORD_COMPLEXITY;

    /**
     * Constructor.
     */
    public DevicePolicy() {
        Log.d(TAG, "DevicePolicy()");
    }

    /**
     * Sets the context of the Command. This can then be used to do things like
     * get file paths associated with the Activity.
     *
     * @param cordova The context of the main Activity.
     * @param webView The CordovaWebView Cordova is running in.
     */
    @Override
    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        Log.i(TAG, "initialize()");
        super.initialize(cordova, webView);

        this.cordova = cordova;
        this.componentName = DevicePolicyReceiver.getComponentName(cordova.getActivity());
        this.devicePolicyManager = (DevicePolicyManager) cordova.getActivity().getSystemService(Context.DEVICE_POLICY_SERVICE);
        this.packageManager = cordova.getActivity().getPackageManager();

        Log.d(TAG, "passwordComplexitySupported: " + Boolean.toString(this.passwordComplexitySupported()));
    }


    private boolean passwordComplexitySupported() {
      return android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q; // SDK=29
    };

    /**
     * Executes the given request.
     *
     * @param action            The action to execute.
     * @param args              JSONArry of arguments for the plugin.
     * @param callbackContext   The callback id used when calling back into JavaScript.
     * @return                  True if the action was valid, false if not.
     */
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        Log.i(TAG, "execute()");
        this.callbackContext = callbackContext;

        if ("getStatus".equals(action)) {
            getStatus();
            return true;
        }
        if ("setAdminActive".equals(action)) {
            setAdminActive();
            return true;
        }
        if ("setSettings".equals(action)) {
            setSettings(args.getInt(0), args.getInt(1));
            return true;
        }
        if ("requestPasswordChange".equals(action)) {
            requestPasswordChange();
            return true;
        }
        if ("setPasswordSufficient".equals(action)) {
            setPasswordSufficient();
            return true;
        }
        if ("setStorageEncryption".equals(action)) {
            setStorageEncryption();
            return true;
        }
        return false;
    }

    /**
     * Returns the device's over all policy status.
     */
    private void getStatus() throws JSONException {
        Log.i(TAG, "getStatus()");
        this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, getStatusInfo()));
    }

    private void setSettings(Integer passMinLength, Integer requiredPasswordComplexity) throws JSONException {
      Log.i(TAG, "setSettings(" + String.valueOf(passMinLength) + ", " + String.valueOf(requiredPasswordComplexity) + ")");

      this.REQUIRED_PASSWORD_COMPLEXITY = requiredPasswordComplexity;

      this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, getStatusInfo()));
    }

    private boolean isAdminActive() {
      return this.devicePolicyManager.isAdminActive(this.componentName);
    }

    private JSONArray getAvailableFeatures() {
      Log.i(TAG, "getAvailableFeatures()");
      final String[] features = {
        PackageManager.FEATURE_FINGERPRINT,
        PackageManager.FEATURE_FACE,
        PackageManager.FEATURE_IRIS
      };
      JSONArray availableFeatures = new JSONArray();

      for (String feature : features) { 
        if (this.packageManager.hasSystemFeature(feature)) {
          availableFeatures.put(feature);
        }
      }

      return availableFeatures;
  }

    /**
     * Returns information for the device's over all policy status.
     */
    private JSONObject getStatusInfo() throws JSONException {
        Log.i(TAG, "getStatusInfo()");

        JSONObject json = new JSONObject();
        json.put("isAdminActive", this.isAdminActive());
        json.put("isPasswordSufficient", this.isPasswordSufficient());
        json.put("encryptionStatus", this.devicePolicyManager.getStorageEncryptionStatus());
        json.put("sdkVersion", android.os.Build.VERSION.SDK_INT);
        json.put("availableFeatures", this.getAvailableFeatures());

        // Prevent "No virtual method getPasswordComplexity" in older SDK
        if (this.passwordComplexitySupported()) {
          json.put("passwordComplexity", this.devicePolicyManager.getPasswordComplexity());
        }

        Log.i(TAG, "getStatusInfo: " + json.toString());

        return json;
    }

    /**
     * Handles a setAdminActive() request
     * If this Android component is not under administration then starts the enabling process.
     * If the device password is insufficient then starts the change/set password process.
     * Otherwise all ok..
     */
    private void setAdminActive() throws JSONException {
        Log.i(TAG, "setAdminActive()");
        if (!this.devicePolicyManager.isAdminActive(this.componentName)) {
            PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true);
            this.callbackContext.sendPluginResult(pluginResult);
            startAdminActive();
        } else {
          this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, getStatusInfo()));
        }
    }

    /**
     * Handles a setPasswordSufficient() request
     * If this Android component is not under administration then starts the enabling process.
     * If the device password is insufficient then starts the change/set password process.
     * Otherwise all ok..
     */
    private void setPasswordSufficient() throws JSONException {
        Log.i(TAG, "setPasswordSufficient()");

        if (!this.devicePolicyManager.isActivePasswordSufficient()) {
            PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
            pluginResult.setKeepCallback(true);
            this.callbackContext.sendPluginResult(pluginResult);
            startPasswordSufficient();
        } else {
          this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, getStatusInfo()));
        }
    }

    /**
     * Check if password complexity is sufficient
     */
    private boolean isPasswordSufficient() {
      Log.i(TAG, "isPasswordSufficient()");

      if (!this.passwordComplexitySupported()) {
        // For legacy SDK/Android, where getPasswordComplexity is not available,
        // always consider password suffiecient
        return true; 
      }

      int currentComplexity =  this.devicePolicyManager.getPasswordComplexity();
      // MEDIUM=196608, HIGH=327680
      Log.i(TAG, "currentComplexity: " + Integer.toString(currentComplexity));
      Log.i(TAG, "REQUIRED_PASSWORD_COMPLEXITY: " + Integer.toString(this.REQUIRED_PASSWORD_COMPLEXITY));

      // Compare numerically (see https://developer.android.com/reference/android/app/admin/DevicePolicyManager#PASSWORD_COMPLEXITY_HIGH)
      return currentComplexity >= this.REQUIRED_PASSWORD_COMPLEXITY;
    }

    /**
     * Trigger password change prompt via intent
     */
    private void requestPasswordChange() throws JSONException {
        Log.i(TAG, "requestPasswordChange()");

        Intent intent = new Intent(DevicePolicyManager.ACTION_SET_NEW_PASSWORD);
        intent.putExtra(DevicePolicyManager.EXTRA_PASSWORD_COMPLEXITY, this.REQUIRED_PASSWORD_COMPLEXITY);
        this.cordova.startActivityForResult(this, intent, REQUEST_NEW_PASSWORD);

        this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, getStatusInfo()));
    }

    /**
     * Checks the device's encryption status and launches encryption when needed.
     * If encryption is active then we ensure Device Admin is enabled and a Password set.
     */
    private void setStorageEncryption() throws JSONException {
        Log.i(TAG, "setStorageEncryption()");
        switch (this.devicePolicyManager.getStorageEncryptionStatus()) {
            case DevicePolicyManager.ENCRYPTION_STATUS_ACTIVE:
            case DevicePolicyManager.ENCRYPTION_STATUS_ACTIVE_PER_USER:
            case DevicePolicyManager.ENCRYPTION_STATUS_ACTIVE_DEFAULT_KEY:
                setAdminActive();
                return;
            case DevicePolicyManager.ENCRYPTION_STATUS_INACTIVE:
                PluginResult pluginResult = new PluginResult(PluginResult.Status.NO_RESULT);
                pluginResult.setKeepCallback(true);
                this.callbackContext.sendPluginResult(pluginResult);
                startStorageEncryption();
                return;
            case DevicePolicyManager.ENCRYPTION_STATUS_ACTIVATING:
                Log.e(TAG, "setStorageEncryption() Encryption is activating on this device.");
                this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, getStatusInfo()));
                return;
            case DevicePolicyManager.ENCRYPTION_STATUS_UNSUPPORTED:
                Log.e(TAG, "setStorageEncryption() Encryption is not supported on this device.");
                this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, getStatusInfo()));
                return;
            default:
                Log.e(TAG, "setStorageEncryption() Unknown storage encryption status: " + this.devicePolicyManager.getStorageEncryptionStatus());
                this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, getStatusInfo()));
        }
    }

    /**
     * Invokes the Android DevicePolicyManager to put this component under administration
     */
    private void startAdminActive() {
        Log.i(TAG, "startAdminActive()");
        Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, this.componentName);
        this.cordova.startActivityForResult(this, intent, REQUEST_ADMIN_ACTIVE);
    }

    /**
     * Invokes the Android DevicePolicyManager to ensure the device password meets requirements as set in
     * DevicePolicyReceiver.onEnabled().
     */
    private void startPasswordSufficient() {
        Log.i(TAG, "startPasswordSufficient()");
        this.cordova.startActivityForResult(this, new Intent(DevicePolicyManager.ACTION_SET_NEW_PASSWORD), REQUEST_NEW_PASSWORD);
    }

    /**
     * Invokes the Android DevicePolicyManager to encrypt the device storage
     */
    private void startStorageEncryption() {
        Log.i(TAG, "startStorageEncryption()");
        this.cordova.startActivityForResult(this, new Intent(DevicePolicyManager.ACTION_START_ENCRYPTION), REQUEST_STORAGE_ENCRYPTION);
    }

    /**
     * Callback method used by Android after startEnable() and startChange() activities complete.
     *
     * @param requestCode   The request code originally supplied to startActivityForResult(),
     *                      allowing you to identify who this result came from.
     * @param resultCode    The integer result code returned by the child activity through its setResult().
     * @param intent        An Intent, which can return result data to the caller
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        Log.i(TAG, "onActivityResult()");

        try {
            switch (requestCode) {
                case REQUEST_ADMIN_ACTIVE:
                  if (resultCode != Activity.RESULT_OK) {
                      Log.w(TAG, "onActivityResult() Admin activation failed to complete");
                      this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, getStatusInfo()));
                  } else if (!this.devicePolicyManager.isActivePasswordSufficient()) {
                      startPasswordSufficient();
                  } else {
                    this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, getStatusInfo()));
                  }
                  return;
                case REQUEST_NEW_PASSWORD:
                    if (resultCode != Activity.RESULT_OK) {
                        Log.w(TAG, "onActivityResult() Password setup failed to complete");
                        this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, getStatusInfo()));
                    } else {
                      this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, getStatusInfo()));
                    }
                    return;
                case REQUEST_STORAGE_ENCRYPTION:
                    if (resultCode != Activity.RESULT_OK) {
                        Log.w(TAG, "onActivityResult() Encryption failed to complete");
                        this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, getStatusInfo()));
                    } else if (!this.devicePolicyManager.isAdminActive(this.componentName)) {
                        startAdminActive();
                    } else {
                      this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, getStatusInfo()));
                    }
                    return;
                default:
                    Log.w(TAG, "onActivityResult() Unexpected requestCode=" + requestCode);
                    this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR, getStatusInfo()));
            }
        } catch (JSONException e) {
            Log.e(TAG, "onActivityResult() Error building json response: ", e);
            this.callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.JSON_EXCEPTION, "Error building json response: " + e));
        }
    }
}
