package org.peekvision.cordova.devicepolicy;

import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class DevicePolicyReceiver extends DeviceAdminReceiver {
    private static final String TAG = "DevicePolicyReceiver";

    @Override
    public void onEnabled(Context context, Intent intent) {
        Log.i(TAG, "onEnabled()");
        super.onEnabled(context, intent);

        DevicePolicyManager devicePolicyManager = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        ComponentName componentName = getComponentName(context);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) { // Q=A10
          // Do nothing, password change, if required will be requested via Capture
        } else {
          devicePolicyManager.setPasswordQuality(componentName, DevicePolicyManager.PASSWORD_QUALITY_NUMERIC_COMPLEX);
        }
    }

    public static ComponentName getComponentName(Context context) {
        Log.i(TAG, "getComponentName()");
        return new ComponentName(context, DevicePolicyReceiver.class);
    }
}
