// Type definitions for Apache Cordova Device Policy plugin
// Project: https://github.com/PeekVision/cordova-plugin-device-policy
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped
// 
// Copyright (c) Peek Vision Ltd
// Licensed under the MIT license 

/**
 * This plugin defines a global devicepolicy object, which provides access to management policies enforced on a device.
 * Although the object is in the global scope, it is not available until after the deviceready event.
 */
interface DevicePolicy {

    EncryptionStatus: EncryptionStatusType;

    /**
     * Gets the device policy statuses:
     *  isAdminActive
     *  isPasswordSufficient
     *  encryptionStatus
     */
    getStatus;

    /**
     * Ensures the administrator is currently active (enabled) in the system.
     */
    setAdminActive;

    /**
     * Ensures a password/pin is set and sufficient.
     */
    setPasswordSufficient;

    /**
     * Starts the device's encryption process
     */
    setStorageEncryption;
}

interface EncryptionStatusType {
    UNSUPPORTED:        Number;
    INACTIVE:           Number;
    ACTIVATING:         Number;
    ACTIVE:             Number;
    ACTIVE_DEFAULT_KEY: Number;
    ACTIVE_PER_USER:    Number;
}

declare let devicepolicy: DevicePolicy;