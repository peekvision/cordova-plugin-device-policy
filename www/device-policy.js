/*
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 *
*/
var argscheck = require('cordova/argscheck'),
    exec = require('cordova/exec');

function DevicePolicy() {
    EncryptionStatus = {
        UNSUPPORTED: 0,
        INACTIVE:    1,
        ACTIVATING:  2,
        ACTIVE:      3,
        ACTIVE_DEFAULT_KEY: 4,
        ACTIVE_PER_USER: 5
    };
    DevicePolicy.prototype.getStatus = function (successCallback, errorCallback) {
        argscheck.checkArgs('FF', 'DevicePolicy.getStatus', arguments);
        exec(successCallback, errorCallback, "DevicePolicy", "getStatus");
    };
    DevicePolicy.prototype.setAdminActive = function (successCallback, errorCallback) {
        argscheck.checkArgs('FF', 'DevicePolicy.setAdminActive', arguments);
        exec(successCallback, errorCallback, "DevicePolicy", "setAdminActive", null);
    };
    DevicePolicy.prototype.setSettings = function (successCallback, errorCallback, passMinLength, requiredPasswordComplexity) {
      requiredPasswordComplexity = requiredPasswordComplexity ? requiredPasswordComplexity : 196608;  // MEDIUM
      argscheck.checkArgs('FFNN', 'DevicePolicy.setSettings', arguments);
        exec(successCallback, errorCallback, "DevicePolicy", "setSettings", [passMinLength, requiredPasswordComplexity]);
    };
    DevicePolicy.prototype.setPasswordSufficient = function (successCallback, errorCallback) {
        argscheck.checkArgs('FF', 'DevicePolicy.setPasswordSufficient', arguments);
        exec(successCallback, errorCallback, "DevicePolicy", "setPasswordSufficient");
    };
    DevicePolicy.prototype.requestPasswordChange = function (successCallback, errorCallback) {
        argscheck.checkArgs('FF', 'DevicePolicy.requestPasswordChange', arguments);
        exec(successCallback, errorCallback, "DevicePolicy", "requestPasswordChange");
    };
    DevicePolicy.prototype.setStorageEncryption = function (successCallback, errorCallback) {
        argscheck.checkArgs('FF', 'DevicePolicy.setStorageEncryption', arguments);
        exec(successCallback, errorCallback, "DevicePolicy", "setStorageEncryption", null);
    };
}

module.exports = new DevicePolicy();



